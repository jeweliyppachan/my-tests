var main = 0;

$(document).ready(function () {
    $('#add_more').click(function () {
        $(this).before($("<div/>",
            {
                id: 'filediv'
            }).append($("<input/>",

                {
                    name: 'file[]',
                    type: 'file',
                    id: 'file'
                }), $("<br/><br/>")));

    });
    $('body').on('change', '#file', function () {
        if (this.files && this.files[0]) {
            main += 1;
        /*     var z = main - 1; */
            $(this).before("<div id='main" + main + "' class='main'><img id='previewimg" + main + "' src=''/></div>");

            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
            $(this).hide();

        }
    });

    function imageIsLoaded(e) {
        $('#previewimg' + main).attr('src', e.target.result);
    };

    $('#upload').click(function (e) {
        var name = $(":file").val();
        if (!name) {
            alert("First Image Must Be Selected");
            e.preventDefault();
        }
    });
});